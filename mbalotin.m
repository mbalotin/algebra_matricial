% Plotar um poligono

x = [0 2 2 1 1 0 0 ] %repetir o ultimo elemento para fechar o poligono
y = [1 1 2 2 4 4 1 ] %repetir o ultimo elemento para fechar o poligono

%plot(x, y)

% Simetria / Reflešao em relašao a x
T=[1 0 ; 0 -1] %Matriz de tranformašao
M=[x;y] %matriz composta pelos vetores  x e y
N=T*M %Transformacao dos vetices, multiplicando as matrizes
%Extrai os vetores da matriz resultante para que possam ser plotados
x1=N(1,:) 
y1=N(2,:)
%Para plotar uma linha sobre o eixo do x, e 
x2= [0 4]
y2= [0 0]

%plot(x,y, x1,y1, x2,y2)

%Dilatašao (alpha > 1)  e contracao (0 <= alpha <= 1)

x = [0 1 1 0 0]
y = [0 0 1 1 0]

A=[2 0 ; 0 2] %alpha = 2
M=[x;y]

N = A*M

x1=N(1,:) 
y1=N(2,:)

%plot(x,y,x1,y1)

%Cisalhamento
A=[1 3 ; 0 1 ] %Alpha = 3 
M=[x;y]

N = A*M

x1=N(1,:) 
y1=N(2,:)

%plot(x,y,x1,y1)

%Rotacao

A=[cos(pi/4) -1*sin(pi/4) ; sin(pi/4) cos(pi/4) ] % matriz de rotašao para 45║ (pi/4)
M=[x;y]

N = A*M

x1=N(1,:) 
y1=N(2,:)

%plot(x,y,x1,y1)

%Composišao

T1=[cos(pi/4) -1*sin(pi/4) ; sin(pi/4) cos(pi/4) ] % rotacao de 45║
T2=[2 0 ; 0 2] %dilatacao de fator 2
T3=[1 3 ; 0 1 ] %Alpha = 3 

A=T2*T3*T1 % composicao das transformacoes

M=[x;y]
N = A*M
x1=N(1,:) 
y1=N(2,:)

x2= [0 10 10 0 0]
y2= [0 0 10 10 0]

plot(x,y,x1,y1,x2,y2)
