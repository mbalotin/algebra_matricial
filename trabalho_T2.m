%definem a �rea do jogo
a = [1 10 10 1 1]
b = [0 0 10 10 0]

%Pe�a de Retangulo
pecaRetaaxisX = [6 10 10 6 6] 
pecaRetaaxisY = [0 0 1 1 0]
%Pe�a em formato de T
pecaTaxisX = [1 4 4 3 3 2 2 1 1] 
pecaTaxisY = [0 0 1 1 2 2 1 1 0]
%Peca em formato de quadrado
pecaQaxisX = [8 10 10 8 8]
pecaQaxisY = [1 1 3 3 1]

%pe�a que ser� movida pelo tabuleiro
pecaLaxisX = [1; 1; 2; 2; 0; 0; 1; 1] 
pecaLaxisY = [1; 0; 0; 3; 3; 2; 2; 1]

%Posi��o inicial da pe�a em L
pecaLaxisX = pecaLaxisX + 4
pecaLaxisY = pecaLaxisY + 7

%Posi��o inicial
fill( a,b, 'white', pecaLaxisX,pecaLaxisY, '' , pecaRetaaxisX, pecaRetaaxisY, 'red', pecaTaxisX, pecaTaxisY, 'green', pecaQaxisX, pecaQaxisY, 'yellow')
axis equal
pause(1);

%Pe�a come�a a descer
pecaLaxisY = pecaLaxisY -1
fill( a,b, 'white', pecaLaxisX,pecaLaxisY, '' , pecaRetaaxisX, pecaRetaaxisY, 'red', pecaTaxisX, pecaTaxisY, 'green', pecaQaxisX, pecaQaxisY, 'yellow')
axis equal
pause(1);

%Tenta Virar a pe�a 90 graus
aux = [1; 1; 1; 1; 1; 1; 1; 1]
T = [pecaLaxisX , pecaLaxisY , aux]
A = [ cos(-pi/2) -sin(-pi/2) 0 ; sin(-pi/2) cos(-pi/2) 0 ; 13 0 1 ]

J = T * A;

pecaLaxisX = J(:, 1);
pecaLaxisY = J(:, 2);

fill( a,b, 'white', pecaLaxisX,pecaLaxisY, '' , pecaRetaaxisX, pecaRetaaxisY, 'red', pecaTaxisX, pecaTaxisY, 'green', pecaQaxisX, pecaQaxisY, 'yellow')
axis equal;
pause(1);

%desce uma linha 
pecaLaxisY = pecaLaxisY -1
fill( a,b, 'white', pecaLaxisX,pecaLaxisY, '' , pecaRetaaxisX, pecaRetaaxisY, 'red', pecaTaxisX, pecaTaxisY, 'green', pecaQaxisX, pecaQaxisY, 'yellow')
axis equal
pause(1);

%Tenta virar novamente 90 graus
aux = [1; 1; 1; 1; 1; 1; 1; 1]
T = [pecaLaxisX , pecaLaxisY , aux]
A = [ cos(-pi/2) -sin(-pi/2) 0 ; sin(-pi/2) cos(-pi/2) 0 ; 10 -2 1]

J = T * A;

pecaLaxisX = J(:, 1);
pecaLaxisY = J(:, 2);

fill( a,b, 'white', pecaLaxisX,pecaLaxisY, '' , pecaRetaaxisX, pecaRetaaxisY, 'red', pecaTaxisX, pecaTaxisY, 'green', pecaQaxisX, pecaQaxisY, 'yellow')
axis equal;
pause(1);

%move a pe�a para a esquerda enquanto ela desce
pecaLaxisY = pecaLaxisY -1

pecaLaxisX = pecaLaxisX -1
fill( a,b, 'white', pecaLaxisX,pecaLaxisY, '' , pecaRetaaxisX, pecaRetaaxisY, 'red', pecaTaxisX, pecaTaxisY, 'green', pecaQaxisX, pecaQaxisY, 'yellow')
axis equal
pause(1);

%Desde a ultima vez e encaixa a pe�a
pecaLaxisY = pecaLaxisY -1
fill( a,b, 'white', pecaLaxisX,pecaLaxisY, '' , pecaRetaaxisX, pecaRetaaxisY, 'red', pecaTaxisX, pecaTaxisY, 'green', pecaQaxisX, pecaQaxisY, 'yellow')
axis equal
pause(1);
 
%%%% remove a �ltima linha

%Move o quaadrado para baixo
pecaQaxisY = pecaQaxisY - 1
%Redesenha o 'T' sem a cabe�a
pecaTaxisY = [0 0 1 1 0]
pecaTaxisX = [2 3 3 2 2]
%Resenha o 'L' sem a ultima parte
pecaLaxisY = [0 0 2 2 0]
pecaLaxisX = [4 5 5 4 4]

fill( a,b, 'white', pecaLaxisX,pecaLaxisY, '', pecaTaxisX, pecaTaxisY, 'green', pecaQaxisX, pecaQaxisY, 'yellow')
axis equal